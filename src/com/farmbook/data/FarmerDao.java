package com.farmbook.data;

import com.farmbook.model.Farmer;
import com.farmbook.util.DataFetcher;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by anil on 9/2/16.
 */
public class FarmerDao {

    private static Connection dataConnection = DataFetcher.getDataConnection();

    public static Farmer saveFarmer(Farmer farmer){
        if(farmer!=null){
            String query = getQueryForFarmer(farmer);
            try {
                Statement statement = dataConnection.createStatement();
                statement.executeUpdate(query);
                return getFarmerByUsernameAndPassword(farmer.getUsername(),farmer.getPassword());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Farmer getFarmerByUsernameAndPassword(String username, String password){
        Farmer farmer=null;
        String query = "select * from farmer WHERE username='"+username+ "' and password='"+password+"'";
        try {
            Statement statement = dataConnection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                 farmer = getFarmerByResult(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return farmer;
    }

    private static Farmer getFarmerByResult(ResultSet resultSet) throws SQLException {
        if(resultSet!=null){
            Farmer farmer = new Farmer();
            farmer.setId(resultSet.getLong("id"));
            farmer.setName(resultSet.getString("name"));
            farmer.setUsername(resultSet.getString("username"));
            farmer.setPassword(resultSet.getString("password"));
            farmer.setEmailId(resultSet.getString("email_id"));
            farmer.setPhoneNo(resultSet.getString("phone_no"));
            farmer.setAge(resultSet.getInt("age"));
            farmer.setAddress(resultSet.getString("address"));
            farmer.setCrops(resultSet.getString("crops"));
            farmer.setEquipment(resultSet.getString("equipment"));
            farmer.setLocation(resultSet.getString("location"));
            return farmer;
        }
        return null;
    }

    private static String getQueryForFarmer(Farmer farmer){
        String insertQuery="INSERT INTO farmer(name,username,password,email_id,phone_no,age,location,address,crops,equipment) VALUES("
                +getQueryValue(farmer.getName())+","+getQueryValue(farmer.getUsername())+","+getQueryValue(farmer.getPassword())+","
                +getQueryValue(farmer.getEmailId())+","+getQueryValue(farmer.getPhoneNo()) +","+farmer.getAge()+","
                +getQueryValue(farmer.getLocation())+","+getQueryValue(farmer.getAddress())+","+getQueryValue(farmer.getCrops())+","
                +getQueryValue(farmer.getEquipment())+")";
        String updateQuery = "update farmer set name="+getQueryValue(farmer.getName())+" ,email_id="+getQueryValue(farmer.getEmailId())
                +" ,phone_no="+getQueryValue(farmer.getPhoneNo()) + " ,age="+farmer.getAge()+" ,location="+getQueryValue(farmer.getLocation())
                +" ,address=" +getQueryValue(farmer.getAddress()) + " ,crops=" +getQueryValue(farmer.getCrops())+ " ,equipment=" + getQueryValue(farmer.getEquipment())
                +" where id =" + farmer.getId();

        return farmer.getId()==null? insertQuery:updateQuery;
    }

    private static String getQueryValue(String s){
        return s==null? s : "'"+s+"'";
    }
}