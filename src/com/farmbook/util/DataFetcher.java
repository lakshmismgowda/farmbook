package com.farmbook.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by anil on 9/2/16.
 */
public class DataFetcher {

    static Connection conn = null;
    public static Connection getDataConnection(){
        try {
            if(conn==null || conn.isClosed()){
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://localhost/farmbook","farmbook","admin");
            }

        } catch (SQLException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }
}
