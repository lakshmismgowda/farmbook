package com.farmbook.model;

/**
 * Created by anil on 10/2/16.
 */
public class BaseModel {

    String requestType;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
