package com.farmbook.controller;

import com.farmbook.data.FarmerDao;
import com.farmbook.model.Farmer;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anil on 9/2/16.
 */
public class FarmerController extends HttpServlet {

    private  Gson gson = new Gson();

    public void doPost(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException
    {
        BufferedReader reader = request.getReader();
        String line;
        StringBuilder buffer = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        String data = buffer.toString();
        Farmer farmer = gson.fromJson(data, Farmer.class);
        String responseString = "{error:\"no response\"}";
        if(farmer!=null && farmer.getRequestType()!=null){
            switch (farmer.getRequestType()){
                case "signup": responseString = signUpFarmer(farmer); break;
                case "signin": responseString = signInFarmer(farmer); break;
                case "updateprofile": responseString = updateFarmer(farmer); break;
            }

        }

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.println(responseString);
    }

    private String signUpFarmer(Farmer farmer){
        if(farmer!=null && farmer.getId()==null){
            Farmer savedFarmer = FarmerDao.saveFarmer(farmer);
            if(savedFarmer!=null)
                return gson.toJson(savedFarmer);
        }
        return getErrorResponse();
    }

    private String getErrorResponse() {
        return "{\"response\":\"error\"}";
    }

    private String signInFarmer(Farmer farmer){
        if(farmer!=null){
            Farmer loggedInFarmer = FarmerDao.getFarmerByUsernameAndPassword(farmer.getUsername(), farmer.getPassword());
            if(loggedInFarmer!=null)
                return gson.toJson(loggedInFarmer);
        }
        return getErrorResponse();
    }

    private String updateFarmer(Farmer newFarmer){
        if(newFarmer!=null){
            Farmer oldFarmer = FarmerDao.getFarmerByUsernameAndPassword(newFarmer.getUsername(), newFarmer.getPassword());
            if(oldFarmer!=null && oldFarmer.getId()!=null){
                newFarmer.setId(oldFarmer.getId());
                Farmer savedFarmer = FarmerDao.saveFarmer(newFarmer);
                if(savedFarmer!=null)
                    return gson.toJson(savedFarmer);
            }
        }
        return getErrorResponse();
    }

    /*private boolean allowAccess(Farmer farmer) {
        Farmer loggedInFarmer = FarmerDao.getFarmerByUsernameAndPassword(farmer.getUsername(), farmer.getPassword());
        return loggedInFarmer!=null && loggedInFarmer.getId()!=null;
    }*/
}
